#!/bin/bash

#Initiera hela programmet med en while-loop som håller menyn igång
#Använd case-satser för menyn, använd read för att läsa val

#Color variables for altering text outputs
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' #No color / reset

#Global universal variabel
USERCHOICE=""
GLOBALTMP=""


waitForEnter(){
	SCRAP=""
	echo ""
	echo ""
	echo "Press enter to continue..."
	read SCRAP
}

groupAdd(){
	clear
	echo "Enter name of new group: "
	read USERCHOICE
	sudo groupadd $USERCHOICE
	waitForEnter
}

groupList(){
	clear
	echo -e "${GREEN}$(getent group | cut -d: -f1)${NC}"
	waitForEnter
}

groupView(){
	clear
	echo "Which group would you like to view?"
	read USERCHOICE
	clear
	if ! (grep -c --quiet "^$USERCHOICE" /etc/group); then
		echo "The group $USERCHOICE does not exist"
	else
		echo "The following users are in group $USERCHOICE"
		echo -e "${GREEN}$(grep "^$USERCHOICE" /etc/group | cut -d: -f4)${NC}"
	fi
	echo ""
	waitForEnter
}

groupModify(){
	clear
	echo "Which group would you like to modify?"
	GROUPNAME=""
	USERNAME=""
	read GROUPNAME
	clear

	echo "**************************************************************"
	echo "          SYSTEM MODIFIER - Modifying group $GROUPNAME"
	echo "--------------------------------------------------------------"
	echo -e "${RED}1 - ${NC}Add user to group"
	echo -e "${RED}2 - ${NC}Remove user from group"
	echo -e "${RED}exit - ${NC}Exit group:modify"
	echo "--------------------------------------------------------------"

	read USERCHOICE

	case $USERCHOICE in
		"1")echo "Which user would you like to add?" && read USERNAME
		sudo usermod -a -G $GROUPNAME $USERNAME && echo "$USERNAME added to $GROUPNAME";;
		"2")echo "Which user would you like to remove?" && read USERNAME
		sudo gpasswd -d $USERNAME $GROUPNAME;;
		"exit") ;;
		*) echo "Sorry, I didn't understand that"
	esac
	waitForEnter
}

userAdd(){
	clear
	echo "Enter name of new user"
	read USERCHOICE
	sudo useradd $USERCHOICE && echo "New user $USERCHOICE added"
	waitForEnter
}

userList(){
	clear
	echo -e "${GREEN}$(cat /etc/passwd | cut -d: -f1)${NC}"
	waitForEnter
}


userViewExist(){
	clear

	echo "**************************************************************"
	echo "          SYSTEM MODIFIER - Viewing user $USERCHOICE"
	echo "--------------------------------------------------------------"
	echo -e "${GREEN}Username${NC}		$USERCHOICE"
	echo -e "${GREEN}Password${NC}		$(getent passwd $USERCHOICE | cut -d: -f2)"
	echo -e "${GREEN}User ID${NC} 		$(id -u $USERCHOICE)"
	echo -e "${GREEN}Group ID${NC}		$(id -g $USERCHOICE)"
	echo -e "${GREEN}Comment${NC} 		$(getent passwd $USERCHOICE | cut -d: -f5)"
	echo -e "${GREEN}Directory${NC}		$(getent passwd $USERCHOICE | cut -d: -f6)"
	echo -e "${GREEN}Shell${NC}   		$(getent passwd $USERCHOICE | cut -d: -f7)"
	echo "--------------------------------------------------------------"
}

userView(){
        clear
	echo -e "${GREEN}$(cat /etc/passwd | cut -d: -f1)${NC}"
	echo ""
	echo "Which user would you like to view?"
	read USERCHOICE

	case $(grep -c $USERCHOICE /etc/passwd) in
		"0") echo "User $USERCHOICE does not exist";;
		"1") userViewExist;;
	esac

	waitForEnter
}

userModify(){
        clear
        echo -e "${GREEN}$(cat /etc/passwd | cut -d: -f1)${NC}"
        echo ""
	echo "Which user would you like to modify?"
	read USERCHOICE
	case $(grep -c $USERCHOICE /etc/passwd) in
		"0") echo "user $USERCHOICE does not exist" && waitForEnter  && return ;;
		"1") userViewExist
	esac

	echo "**************************************************************"
	echo "          SYSTEM MODIFIER - Modifying user $USERCHOICE"
	echo "--------------------------------------------------------------"
	echo -e "${RED}1 - ${NC}Change username"
	echo -e "${RED}2 - ${NC}Change password"
	echo -e "${RED}3 - ${NC}Change user ID"
	echo -e "${RED}4 - ${NC}Change group ID"
	echo -e "${RED}5 - ${NC}Change user comment"
	echo -e "${RED}6 - ${NC}Change default directory"
	echo -e "${RED}7 - ${NC}Change default shell"
	echo -e "${RED}exit - ${NC}Exit user:modify"
	echo "--------------------------------------------------------------"

	NEWCHOICE=""
	OLDCHOICE=$USERCHOICE

	read USERCHOICE
	case $USERCHOICE in
		"1") echo "Enter new username"
		read NEWCHOICE
		sudo usermod -l $NEWCHOICE $OLDCHOICE && echo "Username $OLDCHOICE changed to $NEWCHOICE";;

		"2") echo "Enter new password for $OLDCHOICE"
		#read NEWCHOICE
		sudo passwd $OLDCHOICE && echo "Password succsefully changed";;

		"3") echo "Enter new user ID for $OLDCHOICE"
		read NEWCHOICE
		sudo usermod -u $NEWCHOICE $OLDCHOICE && echo "User ID succesufully changed";;

		"4") echo "Enter new group ID for $OLDCHOICE"
		read NEWCHOICE
		sudo usermod -g $NEWCHOICE $OLDCHOICE && echo "Group ID succesfully changed";;

		"5") echo "Enter new comment for $OLDCHOICE"
		read NEWCHOICE
		sudo usermod -c "$NEWCHOICE" $OLDCHOICE && echo "Comment succesfully changed";;

		"6") echo "Enter path to new default directory "
		read NEWCHOICE
		sudo usermod -m -d $NEWCHOICE $OLDCHOICE && echo "Default directory changed to $NEWCHOICE" ;;

		"7") echo "Enter new default shell for $OLDCHOICE"
		read NEWCHOICE
		sudo usermod -s $NEWCHOICE $OLDCHOICE && echo "Default shell changed to $NEWCHOICE";;

		"exit") return;;

		*) echo "Sorry, I didn't understand that"
	esac
	waitForEnter
}

folderAdd(){
	clear
	echo "Enter name for new folder"
	read USERCHOICE
	mkdir $USERCHOICE && echo "Created folder $USERCHOICE"
}

folderList(){
	clear
	echo -e "${BLUE}$(ls -d */)${NC}"

	echo "Select folder"
	read USERCHOICE

	clear
	echo "Contents of $USERCHOICE"
	ls $USERCHOICE
	waitForEnter
}

folderViewExist(){
	clear

	echo "**************************************************************"
	echo "          SYSTEM MODIFIER - Viewing folder $GLOBALTMP"
	echo "--------------------------------------------------------------"
	echo -e "${GREEN}Folder name${NC}		$GLOBALTMP"
	echo -e "${GREEN}Permissions${NC}		$(stat -c %A $GLOBALTMP)"
	echo -e "${GREEN}Folder owner${NC} 		$(stat -c %U $GLOBALTMP)"
	echo -e "${GREEN}Last modified${NC} 		$(stat -c %y $GLOBALTMP)"
	echo "--------------------------------------------------------------"
}

folderView(){
	clear
	echo -e "${BLUE}$(ls -d */)${NC}"

	echo "Select folder"
	read GLOBALTMP

	if [ ! -d "$GLOBALTMP" ]; then
		echo "The folder $GLOBALTMP does not exist"
	else
		folderViewExist
	fi

	waitForEnter
}

folderModifyOwner(){
	folderViewExist
	echo ""
	echo ""

	echo "Enter new owner of $GLOBALTMP"
	read NEWCHOICE
	sudo chown -R $NEWCHOICE $GLOBALTMP && echo "Owner changed to $NEWCHOICE"
	waitForEnter
}

privilegeChoice(){
	TEMP=""
	NEWRUNNING=0

	#Sets the user choice to corresponding letter for code
	case $NEWCHOICE in
		"1") TEMP=u ;;
		"2") TEMP=g ;;
 		"3") TEMP=o ;;
	esac

	while [ $NEWRUNNING == 0 ]
	do
	clear
	folderViewExist
	echo ""
	echo ""

	echo "**************************************************************"
	echo "        SYSTEM MODIFIER - Modifying folder $GLOBALTMP"
	echo "--------------------------------------------------------------"
	echo -e "${RED}1 - ${NC}Enable read privileges"
	echo -e "${RED}2 - ${NC}Enable write privileges"
	echo -e "${RED}3 - ${NC}Enable execute privileges"
	echo ""
	echo -e "${RED}4 - ${NC}Disable read privileges"
	echo -e "${RED}5 - ${NC}Disable write privileges"
	echo -e "${RED}6 - ${NC}Disable execute privileges"
	echo ""
	echo -e "${RED}exit - ${NC}Exit menu"
	echo "--------------------------------------------------------------"
	read NEWCHOICE
	case $NEWCHOICE in
		"1") sudo chmod -R $TEMP+r $GLOBALTMP && echo "Privileges has been changed for $USERCHOICE";;
		"2") sudo chmod -R $TEMP+w $GLOBALTMP && echo "Privileges has been changed for $USERCHOICE";;
		"3") sudo chmod -R $TEMP+x $GLOBALTMP && echo "Privileges has been changed for $USERCHOICE";;
		"4") sudo chmod -R $TEMP-r $GLOBALTMP && echo "Privileges has been changed for $USERCHOICE";;
		"5") sudo chmod -R $TEMP-w $GLOBALTMP && echo "Privileges has been changed for $USERCHOICE";;
		"6") sudo chmod -R $TEMP-x $GLOBALTMP && echo "Privileges has been changed for $USERCHOICE";;
		"exit")((NEWRUNNING++));;
		*) echo "Sorry I didn't understand that.";;
	esac
	done


}

folderModifyPrivilege(){
	clear
	folderViewExist
	echo ""
	echo ""

	echo "**************************************************************"
	echo "        SYSTEM MODIFIER - Modifying folder $GLOBALTMP"
	echo "--------------------------------------------------------------"
	echo -e "${RED}1 - ${NC}Change privilege for owner"
	echo -e "${RED}2 - ${NC}Change privilege for group"
	echo -e "${RED}3 - ${NC}Change privilege for other"
	echo ""
	echo -e "${RED}exit - ${NC}Exit menu"
	echo "--------------------------------------------------------------"

	read NEWCHOICE

	case $NEWCHOICE in
		"1")	privilegeChoice;;
		"2")	privilegeChoice;;
		"3")	privilegeChoice;;
		"exit") return;;
		*) echo "Sorry, I didn't understand that"
		waitForEnter;;
	esac
}

folderModify(){
	clear

	echo -e "${BLUE}$(ls -d */)${NC}"
	echo "Select folder"
	read GLOBALTMP

	clear
	folderViewExist
	echo ""
	echo ""

	echo "**************************************************************"
	echo "        SYSTEM MODIFIER - Modifying folder $GLOBALTMP"
	echo "--------------------------------------------------------------"
	echo -e "${RED}1 - ${NC}Change owner"
	echo -e "${RED}2 - ${NC}Change folder privilages"
	echo -e "${RED}3 - ${NC}Set folder sticky bit"
	echo -e "${RED}4 - ${NC}Set gid for folder"
	echo -e "${RED}5 - ${NC}Change last modified time stamp"
	echo ""
	echo -e "${RED}exit - ${NC}Exit menu"
	echo "--------------------------------------------------------------"
	read USERCHOICE
	case $USERCHOICE in
		"1") folderModifyOwner;;
		"2") folderModifyPrivilege;;

		"3") sudo chmod o+t $GLOBALTMP && echo "Sticky bit set for $GLOBALTMP" || echo "Failed to set sticky"
		waitForEnter;;

		"4") sudo chmod g+s $GLOBALTMP && echo "Set group ID for $GLOBALTMP" || echo "Failed to set group ID"
		waitForEnter;;

		"5") echo "Enter new date (YYYYMMDDHHmm) for $GLOBALTMP"
		read USERCHOICE
		touch -t $USERCHOICE $GLOBALTMP && echo "Timestamp for $GLOBALTMP changed to $USERCHOICE"
		stat $GLOBALTMP | tail -3
		waitForEnter;;

		"exit") return;;
		*) echo "Sorry, I didn't understand that"
		waitForEnter;;
	esac
}

RUNNING=0
while [ $RUNNING == 0 ]
do
	clear
	echo "**************************************************************"
	echo "                      SYSTEM MODIFIER"
	echo "--------------------------------------------------------------"
	echo -e "${RED}1 - ${NC}Create a new group"
	echo -e "${RED}2 - ${NC}List system groups"
	echo -e "${RED}3 - ${NC}List user associations for group"
	echo -e "${RED}4 - ${NC}Modify user associations for group"
	echo ""
	echo -e "${RED}5 - ${NC}Create new user"
	echo -e "${RED}6 - ${NC}List system users"
	echo -e "${RED}7 - ${NC}View user properties"
	echo -e "${RED}8 - ${NC}Modify user properties"
	echo ""
	echo -e "${RED}9 - ${NC}Create a new folder"
	echo -e "${RED}10 - ${NC}List folder contents"
	echo -e "${RED}11 - ${NC}View folder properties"
	echo -e "${RED}12 - ${NC}Modify folder properties"
	echo ""
	echo -e "${RED}13 - ${NC}Install SSH server"
	echo -e "${RED}14 - ${NC}Uninstall SSH server"
	echo -e "${RED}15 - ${NC}Start the SSH server"
	echo -e "${RED}16 - ${NC}Stop SSH server"
	echo ""
	echo -e "${RED}exit - ${NC}Exit program"
	echo "---------------------------------------------------------------"

	read USERCHOICE
	echo ""

	case $USERCHOICE in
		"1") groupAdd;;
		"2") groupList;;
		"3") groupView;;
		"4") groupModify;;
		"5") userAdd;;
		"6") userList;;
		"7") userView;;
		"8") userModify;;
		"9") folderAdd;;
		"10") folderList;;
		"11") folderView;;
		"12") folderModify;;
		"13") sudo apt install openssh-server
		waitForEnter;;
		"14") sudo apt remove openssh-server
		waitForEnter;;

		"15") sudo systemctl start sshd.service
		sudo systemctl status sshd.service | head -3 | tail -1
		waitForEnter;;

		"16") sudo systemctl stop sshd.service
		sudo systemctl status sshd.service | head -3 | tail -1
		waitForEnter;;

		"exit")((RUNNING++));;
		*) echo "Sorry, I didn't understand that"
		waitForEnter;;
	esac
done
